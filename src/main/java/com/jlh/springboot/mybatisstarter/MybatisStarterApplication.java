package com.jlh.springboot.mybatisstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class MybatisStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybatisStarterApplication.class, args);
	}

}
