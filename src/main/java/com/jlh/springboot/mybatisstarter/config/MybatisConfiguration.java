package com.jlh.springboot.mybatisstarter.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/4/2 9:50
 */

@Configuration
@EnableTransactionManagement
@EnableConfigurationProperties(MybatisConfigProperties.class)
@ConditionalOnProperty(value = "system.mybatis.driver",havingValue = "com.mysql.jdbc.Driver")
public class MybatisConfiguration {

    @Bean
    public DataSource dataSourceBean(MybatisConfigProperties mybatisConfigProperties){

        DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(mybatisConfigProperties.getUrl());
        datasource.setUsername(mybatisConfigProperties.getUsername());
        datasource.setPassword(mybatisConfigProperties.getPassword());
        datasource.setDriverClassName(mybatisConfigProperties.getDriver());
        datasource.setInitialSize(5);
        datasource.setMinIdle(5);
        datasource.setMaxActive(10);
        datasource.setMaxWait(5000);
        datasource.setTimeBetweenEvictionRunsMillis(5000);
        datasource.setMinEvictableIdleTimeMillis(30000);
        datasource.setValidationQuery("select 1;");
        return datasource;
    }

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactoryBean(DataSource dataSource,MybatisConfigProperties mybatisConfigProperties) {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);

        //分页插件
        Properties properties = new Properties();
        properties.setProperty("reasonable", "true");
        properties.setProperty("supportMethodsArguments", "true");
        properties.setProperty("returnPageInfo", "check");
        properties.setProperty("params", "count=countSql");

        //添加XML目录
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            bean.setMapperLocations(resolver.getResources(mybatisConfigProperties.getResourcePath()));
            return bean.getObject();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    @Bean
    public PlatformTransactionManager annotationDrivenTransactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
