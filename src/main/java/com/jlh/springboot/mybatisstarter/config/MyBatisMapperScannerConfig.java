package com.jlh.springboot.mybatisstarter.config;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * On 2016/12/23 0023.
 *
 * @description Mybatis 扫包配置
 */
//@Configuration
////注意，由于MapperScannerConfigurer执行的比较早，所以必须有下面的注解
//@AutoConfigureAfter(MybatisConfiguration.class)
//public class MyBatisMapperScannerConfig {
//
//    @Bean
//    public static MapperScannerConfigurer mapperScannerConfigurer() {
//        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
//        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
//        mapperScannerConfigurer.setBasePackage("com.jlh.springboot.mybatisstarter.mapper.**");
//        return mapperScannerConfigurer;
//    }
//}
