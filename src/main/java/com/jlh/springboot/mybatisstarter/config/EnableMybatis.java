package com.jlh.springboot.mybatisstarter.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * com.jlh.springboot.mybatisstarter.config
 *
 * @author mymx.jlh
 * @date 2018/4/8 18:01
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(MybatisConfiguration.class)
public @interface EnableMybatis {
}
